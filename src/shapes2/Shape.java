package shapes2;

public abstract class Shape {
    protected char drawChar;

    public abstract void move(double x, double y);

    public abstract void draw(Screen screen);
}
