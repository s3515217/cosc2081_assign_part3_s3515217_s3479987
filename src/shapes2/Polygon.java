package shapes2;

import java.util.ArrayList;

public class Polygon extends Polyline {
    public Polygon(ArrayList<Point> pointList) {
        super(pointList);
    }

    public Polygon(ArrayList<Point> pointList, char drawChar) {
        super(pointList, drawChar);
    }

    public Polygon(Point... point) {
        super(point);
    }

    public Polygon(char drawChar, Point... point) {
        super(drawChar, point);
    }

    @Override
    public void draw(Screen screen) {
        super.draw(screen);
        Point start = getPointList().get(0);
        Point end = getPointList().get(getPointList().size()-1);
        new Line(end,start,drawChar).draw(screen);
    }

    @Override
    public String toString() {
        String string = "";
        for (int i = 0; i < getPointList().size(); i++) {
            string = string + getPointList().get(i) + "\n";
        }
        return "Polygon:\n" + string;
    }
}
