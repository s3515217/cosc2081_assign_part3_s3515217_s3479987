package shapes2;

public class Line extends Shape {
    private Point start;
    private Point end;
    private Boolean switchStartEnd = false;
//    the swtitchStartEnd is for drawing. Have to switch back if TRUE.

    public Line(Point start, Point end) {
        this(start, end, 'L');
    }

    public Line(Point start, Point end, char drawChar) {
        if (start == null || end == null || start.equals(end)) {
            throw new IllegalArgumentException("The 2 points must be different and must not be null.");
        }
        this.start = start;
        this.end = end;
        this.drawChar = drawChar;
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        if (start == null || start.equals(this.end)) {
            throw new IllegalArgumentException("The start point must be different from the end Point and must not be null.");
        }
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        if (end == null || end.equals(this.start)) {
            throw new IllegalArgumentException("The end point must be different from the start Point and must not be null.");
        }
        this.end = end;
    }

    private void switchStartEnd() {
//        Switch Start Point and End Point to draw easier in some cases.
        Point temp = new Point(start.getX(), start.getY());
        start.setX(end.getX());
        start.setY(end.getY());
        end.setX(temp.getX());
        end.setY(temp.getY());
        switchStartEnd = true;
    }

    public double length() {
        return Math.sqrt((start.getX()-end.getX())*start.getX()-end.getX()) + (start.getY()-end.getY())*(start.getY()-end.getY());
    }

    public void move(double x, double y) {
        start.move(x,y);
        end.move(x,y);
    }

    public void draw(Screen screen) {
        //        using y = mx+b
        //        Find the m and b
        double m = (end.getY() - start.getY()) / (end.getX() - start.getX());
        //        m is the slope
        double b = start.getY();
            b = start.getY() - (m * start.getX());
        //        b is y-intercept


//        Check the position between Start Point and End Point
//        Then change position in some cases
        if (start.getX() < end.getX() | start.getY() > end.getY()) {
            if (m < 0 & m > -1) {
//               start lowerleft ; end upperright. Look horizontal.
                switchStartEnd();
            }
        }
        if (start.getX() > end.getX() | start.getY() < end.getY()) {
//                start upperright ; end lowerleft. Look vertical.
            if (m < -1) {
                    switchStartEnd();
            }
        }
        if (start.getX() > end.getX() | start.getY() > end.getY()) {
//                start lowerright ; end upperleft.
                switchStartEnd();
        }


        if (start.getX() == end.getX()) {
            for (int i = 0; i < end.getY() - start.getY() + 1; i++) {
                new Point(start.getX(), start.getY()+i, drawChar).draw(screen);
            }
        }
        if (m <= 1 & m > 0) {
            for (int i = 0; i < end.getX() - start.getX() + 1; i++) {
                new Point(start.getX() + i, m*(start.getX()+i)+b, drawChar).draw(screen);
            }
        }
        if (m <= 0 & m >= -1) {
            for (int i = 0; i < end.getX() - start.getX() + 1; i++) {
                new Point(start.getX() + i, m * (start.getX() + i) + b,drawChar).draw(screen);
            }
        }
        if (m > 1 & start.getX() != end.getX()) {
            for (int i = 0; i < end.getY() - start.getY() + 1; i++) {
                new Point((start.getY() + i - b) / m, start.getY() + i,drawChar).draw(screen);
            }
        }
        if (m <= -1 & start.getX() != end.getX()) {
            for (int i = 0; i < end.getY() - start.getY() + 1; i++) {
                new Point((start.getY() + i - b) / m, start.getY() + i,drawChar).draw(screen);
            }
        }

//        switch back Start and End if previously switched
        if (switchStartEnd) {
            switchStartEnd();
            switchStartEnd = false;
        }
    }

    @Override
    public String toString() {
        return "{" + start + " <--> " + end + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Line line = (Line) o;
        return this.start.equals(line.start) && this.end.equals(line.end)
                || this.start.equals(line.end) && this.end.equals(line.start);
    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }
}
