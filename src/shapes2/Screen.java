package shapes2;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class Screen {
    private char[][] screen;
    private int width;
    private int height;

    public Screen(int width, int height) {
        this.width = width;
        this.height = height;
        screen = new char[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                screen[y][x] = ' ';
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char getChar(int x, int y) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            return screen[y][x];
        }
        return ' ';
    }

    public void setChar(int x, int y, char drawChar) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            screen[y][x] = drawChar;
        }
    }

    public void reset() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                screen[y][x] = ' ';
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('/');
        for (int x = 0; x < width; x++) {
            sb.append('-');
        }
        sb.append("\\\n");

        for (int y = 0; y < height; y++) {
            sb.append('|');
            for (int x = 0; x < width; x++) {
                sb.append(screen[y][x]);
            }
            sb.append("|\n");
        }

        sb.append('\\');
        for (int x = 0; x < width; x++) {
            sb.append('-');
        }
        sb.append("/\n");

        return sb.toString();
    }

    public void print() {
        System.out.println(toString());
    }

    public void print(String filename) {
        PrintWriter pWriter;
        try {
                pWriter = new PrintWriter(filename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        pWriter.print(toString());
        pWriter.close();
    }

    public void printImage(String filename) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (screen[y][x] != ' ') {
                    img.setRGB(x,y,0xffff0000);
//                    If not empty, set color to pixel.
//                    Can also set background color using some other functions.
//                    Or just simply set empty slot with the background color.
//                    But I want to leave a transparent background.
                }
            }
        }
        try {
            ImageIO.write(img, "png", new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

