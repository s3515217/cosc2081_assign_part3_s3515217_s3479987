package shapes2;

public class Square extends Rectangle {

    public Square(Point upperLeft, int size) {
        super(upperLeft, new Point(upperLeft.getX()+size-1, upperLeft.getY()+size-1),'S');
    }

    public Square(Point upperLeft, int size, char drawChar) {
        super(upperLeft, new Point(upperLeft.getX()+size-1, upperLeft.getY()+size-1),drawChar);
    }

    public Square(double x, double y, int size) {
        super(x, y, x+size-1, y+size-1, 'S');

    }

    public Square(double x, double y, int size, char drawChar) {
        super(x, y, x+size-1, y+size-1, drawChar);
    }
}
