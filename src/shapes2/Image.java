    package shapes2;

import java.util.ArrayList;

//Image class does not need to extend Shape because it does not use common attribute with Shape, which is drawChar.
// It just use methods draw and move from each shape.
public class Image {
    private ArrayList<Shape> shapeList;

    public Image(ArrayList<Shape> shapeList) {
        for (int i = 0; i < shapeList.size(); i++) {
            if (shapeList.get(i) == null) {
                throw new IllegalArgumentException("The shapes must not be null.");
            }
        }
        this.shapeList = shapeList;
    }

    public Image(Shape... shape) {
        shapeList = new ArrayList<Shape>();
        shapeList = new ArrayList<Shape>();
        for (int i = 0; i < shape.length; i++) {
            shapeList.add(shape[i]);
        }
    }

    public void move(double x, double y) {
        for (int i = 0; i < shapeList.size(); i++) {
            shapeList.get(i).move(x,y);
        }
    }

    public void draw(Screen screen) {
        for (int i = 0; i < shapeList.size(); i++) {
            shapeList.get(i).draw(screen);
        }
    }
}
