package shapes2;
import java.util.ArrayList;

public class Polyline extends Shape {
    private ArrayList<Point> pointList;

    public Polyline(ArrayList<Point> pointList) {
        this(pointList, 'L');
    }

    public Polyline(ArrayList<Point> pointList, char drawChar) {
        boolean invalid = false;
        for (int i = 0; i < pointList.size()-1; i++) {
            if (pointList.get(i) == null) {
                invalid = true;
            }
            for (int j = i+1; j < pointList.size(); j++) {
                if (pointList.get(i) == pointList.get(j)) {
                    invalid = true;
                }
            }
        }
        if (pointList.get(pointList.size()-1) == null) {
            invalid = true;
        }
        if (invalid) {
            throw new IllegalArgumentException("The points in the array must be different and must not be null.");
        }
        this.pointList = pointList;
        this.drawChar = drawChar;
    }

    public Polyline(Point... point) {
        this( 'L', point);
    }

    public Polyline(char drawChar,Point... point) {
        pointList = new ArrayList<Point>();
        if (point.length < 3) {
            throw new IllegalArgumentException("Must have more than 3 points.");
        }
        for (int i = 0; i < point.length; i++) {
            if (point[i] == null) {
                throw new IllegalArgumentException("The 3 points must not be null.");
            }
            for (int j = 0; j < pointList.size(); j++) {
                if (point[i] == pointList.get(j)) {
                throw new IllegalArgumentException("The points must be different.");
                }
            }
            pointList.add(point[i]);
        }
        this.drawChar = drawChar;
    }

    public void addPoint(Point point) {
        for (int i = 0; i < pointList.size(); i++) {
            if (pointList.get(i) == point | point == null) {
                throw new IllegalArgumentException("The points must be different and must not be null.");
            }
        }
        pointList.add(point);
    }

    public ArrayList<Point> getPointList() {
        return pointList;
    }

    public void move(double x, double y) {
        for (int i = 0; i < pointList.size(); i++) {
            pointList.get(i).move(x,y);
        }
    }

    public void draw(Screen screen) {
        for (int i = 0; i < pointList.size()-1; i++) {
            Point start = pointList.get(i);
            Point end = pointList.get(i+1);
            new Line(start,end).draw(screen);
        }
    }

    @Override
    public String toString() {
        String string = "";
        for (int i = 0; i < pointList.size(); i++) {
            string = string + pointList.get(i) + "\n";
        }
        return "Point List:\n" + string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Polyline polyline = (Polyline) o;

        if (this.pointList.size() != polyline.pointList.size()) {
            return false;
        }

        for (int i = 0; i < this.pointList.size(); i++) {
            if (this.pointList.get(i) != polyline.pointList.get(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return pointList != null ? pointList.hashCode() : 0;
    }
}
