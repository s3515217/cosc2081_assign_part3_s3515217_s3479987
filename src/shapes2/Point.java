package shapes2;

public class Point extends Shape {
    private double x;
    private double y;

    public Point() {
        this(0, 0);
    }

    public Point(double x, double y) {
        this(x, y, 'P');
    }

    public Point(double x, double y, char drawChar) {
        this.x = x;
        this.y = y;
        this.drawChar = drawChar;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Point abs() {
        Point temp = new Point(x, y);
        if (x < 0) {
            temp.setX(-x);
        }
        if (y < 0) {
            temp.setY(-y);
        }
        return temp;
    }

    public double length() {
        return Math.sqrt(x*x + y*y);
    }

    public void move(double x, double y) {
        this.x += x;
        this.y += y;
    }

    public void draw(Screen screen) {
        screen.setChar((int)Math.round(x), (int)Math.round(y), drawChar);
    }

    @Override
    public String toString() {
        return drawChar + "(" + x + ", " + y + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Point point = (Point) o;
        if (point.x != x)
            return false;
        if (point.y != y)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
