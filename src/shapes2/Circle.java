package shapes2;

public class Circle extends Shape {
    private Point center;
    //    rad is radius;
    private int rad;

    public Circle(Point center, int rad) {
        this(center, rad, 'C');
    }

    public Circle(Point center, int rad, char drawChar) {
        if (rad <= 0) {
            throw new IllegalArgumentException("The radius must be larger than 0.");
        }
        this.center = center;
        this.rad = rad;
        this.drawChar = drawChar;
    }

    public Circle(int x, int y, int rad) {
        this(new Point(x,y),rad,'C');
    }

    public Circle(int x, int y, int rad, char drawChar) {
        this(new Point(x,y),rad,drawChar);
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }

    public double getArea() {
        return Math.PI*rad*rad;
    }

    public void move(double x, double y) {
        center.move(x,y);
    }

    public void draw(Screen screen) {
        double h = center.getX();
        double k = center.getY();

        for (int i = (int)(center.getX()-rad); i < center.getX()+rad; i++) {
            for (int j = (int)(center.getY()-rad); j < center.getY()+rad; j++) {

//                Circle Equation: (x – h)^2 + (y – k)^2 = r^2 , Center Point is at (h,k), r is radius
//                The Left side of the equal: (x – h)^2 + (y – k)^2
                double leftSide = (i-h)*(i-h) + (j-k)*(j-k);
//                The Right side of the equal: r^2
                double rightSide = (rad*rad);

                if (leftSide < rightSide) {
                    new Point(i,j,drawChar).draw(screen);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Circle (" +
                "center=" + center +
                ", radius=" + rad +
                ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        if (rad != circle.rad) return false;
        return center.equals(circle.center);

    }

    @Override
    public int hashCode() {
        int result = center.hashCode();
        result = 31 * result + rad;
        return result;
    }
}