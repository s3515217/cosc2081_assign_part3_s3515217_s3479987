package shapes;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by rosatran on 12/14/2016.
 */
public class Screen {
    private char[][] screen;
    private int width;
    private int height;
    private String filename;

    public Screen(int width, int height) {
        this.width = width;
        this.height = height;
        screen = new char[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                screen[y][x] = ' ';
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char getChar(int x, int y) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            return screen[y][x];
        }
        return ' ';
    }

    public void setChar(int x, int y, char drawChar) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            screen[y][x] = drawChar;
        }
    }

    public void reset() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                screen[y][x] = ' ';
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('/');
        for (int x = 0; x < width; x++) {
            sb.append('-');
        }
        sb.append("\\\n");

        for (int y = 0; y < height; y++) {
            sb.append('|');
            for (int x = 0; x < width; x++) {
                sb.append(screen[y][x]);
            }
            sb.append("|\n");
        }

        sb.append('\\');
        for (int x = 0; x < width; x++) {
            sb.append('-');
        }
        sb.append("/\n");

        return sb.toString();
    }

    public void print() {
        System.out.println(toString());
    }

    public void print(String filename) {
        PrintWriter pWriter;
        try {
            pWriter = new PrintWriter(filename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        pWriter.print(toString());
        pWriter.close();
    }

//	Add a printImage method in the Screen class to print the screen into a PNG image. The signature should be
//  public void printImage(String filename);
//  You will probably need to use the classes BufferedImage and ImageIO within that method.

    public void printImage(String filename) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        Graphics2D ig2 = img.createGraphics();
        ig2.setBackground(Color.BLUE);
        ig2.clearRect(0, 0, width, height);

//        for (int x = 0; x < img.getWidth(); x++) {
//            for (int y = 0; y < img.getHeight(); y++) {

            try {
                ImageIO.write(img, "png", new File(filename));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }



