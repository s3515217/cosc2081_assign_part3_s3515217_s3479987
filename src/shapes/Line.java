package shapes;

/**
 * Created by v10983 on 15/11/2016.
 */
public class Line {
    private Point start;
    private Point end;
    private char drawChar;

    public Line(Point start, Point end) {
        this(start, end, 'L');
    }

    public Line(Point start, Point end, char drawChar) {
        if (start == null || end == null || start.equals(end)) {
            throw new IllegalArgumentException("The 2 points must be different and must not be null.");
        }
        this.start = start;
        this.end = end;
        this.drawChar = drawChar;
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        if (start == null || start.equals(this.end)) {
            throw new IllegalArgumentException("The start point must be different from the end Point and must not be null.");
        }
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        if (end == null || end.equals(this.start)) {
            throw new IllegalArgumentException("The end point must be different from the start Point and must not be null.");
        }
        this.end = end;
    }

    public void draw(Screen screen) {
        start.draw(screen);
        end.draw(screen);
    }

    @Override
    public String toString() {
        return "{" + start + " <--> " + end + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Line line = (Line) o;


        // if (!start.equals(line.start)) return false;
        // return end.equals(line.end);


//        // first case: start equals start and end equals end
//        if (this.start.equals(line.start) && this.end.equals(line.end)) {
//            return true;
//        }
//        // second case: start equals end and end equals start
//        if (this.start.equals(line.end) && this.end.equals(line.start)) {
//            return true;
//        }

//        if (this.start.equals(line.start) && this.end.equals(line.end)
//                || this.start.equals(line.end) && this.end.equals(line.start)) {
//            return true;
//        }
//        return false;

        return this.start.equals(line.start) && this.end.equals(line.end)
                || this.start.equals(line.end) && this.end.equals(line.start);
    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }
}
