package shapes;

/**
 * Created by v10983 on 22/11/2016.
 */
public class Rectangle {
    private Point upperLeft;
    private Point lowerRight;

    public Rectangle(Point upperLeft, Point lowerRight) {
        if (upperLeft == null || lowerRight == null ||
                upperLeft.getX() == lowerRight.getX() ||
                upperLeft.getY() == lowerRight.getY()) {
            throw new IllegalArgumentException("The 2 points must have different vertical and horizontal coordinates and must not be null.");
        }
        if (lowerRight.getX() < upperLeft.getX()) {
            double temp = upperLeft.getX();
            upperLeft.setX(lowerRight.getX());
            lowerRight.setX(temp);
        }
        if (lowerRight.getY() < upperLeft.getY()) {
            double temp = upperLeft.getY();
            upperLeft.setY(lowerRight.getY());
            lowerRight.setY(temp);
        }
        this.upperLeft = upperLeft;
        this.lowerRight = lowerRight;
    }

    public Rectangle(double x1, double y1, double x2, double y2) {
//        this(new Point(x1, y1), new Point(x2, y2));
        if (x1 == x2 || y1 == y2) {
            throw new IllegalArgumentException("The 2 points must have different vertical and horizontal coordinates");
        }
        if (x2 < x1) {
            double temp = x1;
            x1 = x2;
            x2 = temp;
        }
        if (y2 < y1) {
            double temp = y1;
            y1 = y2;
            y2 = temp;
        }
        this.upperLeft = new Point(x1, y1);
        this.lowerRight = new Point(x2, y2);
    }

    public Point getUpperLeft() {
        return upperLeft;
    }

    public void setUpperLeft(Point upperLeft) {
        this.upperLeft = upperLeft;
    }

    public Point getLowerRight() {
        return lowerRight;
    }

    public void setLowerRight(Point lowerRight) {
        this.lowerRight = lowerRight;
    }

    public String toString() {
        return "Rectangle{" + upperLeft + lowerRight + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (!upperLeft.equals(rectangle.upperLeft)) return false;
        return lowerRight.equals(rectangle.lowerRight);

    }

    @Override
    public int hashCode() {
        int result = upperLeft.hashCode();
        result = 31 * result + lowerRight.hashCode();
        return result;
    }
}
