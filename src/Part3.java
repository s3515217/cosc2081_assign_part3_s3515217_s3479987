import shapes2.*;

import java.util.ArrayList;

public class Part3 {
    public static void main(String[] args) {
        Screen screen = new Screen(100, 50);

//        test Point
        Point p0 = new Point();
        Point p1 = new Point(50,0);
        Point p2 = new Point(20,10,'c');

        p0.draw(screen);
        p1.draw(screen);
        p2.draw(screen);

        screen.print();
        screen.reset();


//        Test Line
        Line l0 = new Line(new Point(0,0), new Point(0,10), '0');
        Line l1 = new Line(new Point(0,0), new Point(10,0), '1');
        Line l2 = new Line(new Point(0,0), new Point(10,10), '2');
        Line l3 = new Line(new Point(0,0), new Point(10,5), '3');
        Line l4 = new Line(new Point(0,0), new Point(5,10), '4');
        l0.draw(screen);
        l1.draw(screen);
        l2.draw(screen);
        l3.draw(screen);
        l4.draw(screen);

        Line l5 = new Line(new Point(0,10), new Point(0,0), '5');
        Line l6 = new Line(new Point(10,0), new Point(0,0), '6');
        Line l7 = new Line(new Point(10,10), new Point(0,0), '7');
        Line l8 = new Line(new Point(10,5), new Point(0,0), '8');
        Line l9 = new Line(new Point(5,10), new Point(0,0), '9');
        l5.move(20,0);
        l6.move(20,0);
        l7.move(20,0);
        l8.move(20,0);
        l9.move(20,0);
        l5.draw(screen);
        l6.draw(screen);
        l7.draw(screen);
        l8.draw(screen);
        l9.draw(screen);

        Line lA = new Line(new Point(40,0), new Point(50,0), 'A');
        Line lB = new Line(new Point(50,10), new Point(50,0), 'B');
        Line lC = new Line(new Point(40,10), new Point(50,0), 'C');
        Line lD = new Line(new Point(40,5), new Point(50,0), 'D');
        Line lE = new Line(new Point(45,10), new Point(50,0), 'E');
        lA.draw(screen);
        lB.draw(screen);
        lC.draw(screen);
        lD.draw(screen);
        lE.draw(screen);

        Line lF = new Line(new Point(50,0), new Point(40,0));
        Line lG = new Line(new Point(50,0), new Point(50,10));
        Line lH = new Line(new Point(50,0), new Point(40,10));
        Line lI = new Line(new Point(50,0), new Point(40,5));
        Line lJ = new Line(new Point(50,0), new Point(45,10));
        lF.move(20,0);
        lG.move(20,0);
        lH.move(20,0);
        lI.move(20,0);
        lJ.move(20,0);
        lF.draw(screen);
        lG.draw(screen);
        lH.draw(screen);
        lI.draw(screen);
        lJ.draw(screen);

        screen.print();
        screen.printImage("Line.png");
        screen.reset();


//        Test Rectangle
        Rectangle rec0 = new Rectangle(new Point(10,10), new Point(40,20));
        Rectangle rec1 = new Rectangle(new Point(50,0), new Point(60,30),'V');
        rec0.draw(screen);
        rec1.draw(screen);

        Rectangle rec2 = new Rectangle(new Point(40,15), new Point(10,10));
        Rectangle rec3 = new Rectangle(new Point(50,0), new Point(60,30));
        rec2.move(0,-10);
        rec3.move(20,0);
        rec2.draw(screen);
        rec3.draw(screen);

        screen.print();
        screen.printImage("Rectangle.png");
        screen.reset();


//        Test Square
        Square square1 = new Square(new Point(10,2),5,'X');
        Square square2 = new Square(10,2,5,'V');
        square1.draw(screen);
        square2.move(10,10);
        square2.draw(screen);
        screen.print();
        screen.reset();


//        Test Cirlce
        Circle circle = new Circle(50,10,5);
        circle.draw(screen);
        circle.move(-20,0);
        circle.draw(screen);
        screen.print();
        screen.reset();

//        Test Polyline
        Polyline pline0 = new Polyline('a',p0,p1,p2, new Point(60,18), new Point(80,5));
        pline0.draw(screen);
        screen.print();
        screen.reset();

//        Test Polygon
        Polygon pgon1 = new Polygon('A', p0,p1,p2);
        pgon1.draw(screen);
        screen.print();
        screen.reset();

        pgon1.move(5,0);
        pgon1.draw(screen);
        screen.print();
        screen.reset();

    }
}
